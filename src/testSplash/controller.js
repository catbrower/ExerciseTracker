(() => {
    'use strict';

    angular
        .module('app')
        .controller('TestController', ['$scope', TestController]);

    function TestController($scope) {
        $scope.testVar = 'it works!';
    }
})();
