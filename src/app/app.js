(() => {
    'use strict';

    angular
        .module('app', ['ngMaterial', 'ui.router']);

    angular
        .module('app')
        .config(['$stateProvider', '$locationProvider', '$urlRouterProvider', Config]);

    function Config($stateProvider, $locationProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        let testState = {
            name: 'testSplash',
            url: '/',
            controller: "TestController",
            templateUrl: 'views/testSplash/template.html'
        };

        $stateProvider.state(testState);
    }
})();
