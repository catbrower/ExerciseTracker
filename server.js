let bodyParser = require('body-parser');
let express = require('express');
let http = require('http');
let MongoClient = require('mongodb').MongoClient;

const PORT = 3000;
let app = new express();
app.use("/", express.static("public"));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

try {
    app.listen(PORT);
    console.log("Server listening on port: " + PORT);
} catch(err) {
    console.error('Server failed to start');
    console.error(err);
}
