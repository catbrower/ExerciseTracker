var gulp = require('gulp');
var babel = require('gulp-babel');
var browserify = require('gulp-browserify');
var cleanCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var order = require('gulp-order');
var uglify = require('gulp-uglify');

gulp.task('deps', function() {
    gulp
        .src('src/app/deps.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(browserify())
        .pipe(uglify())
        .pipe(rename('deps.min.js'))
        .pipe(gulp.dest('public'));

    gulp
        .src('node_modules/font-awesome/fonts/*.*')
        .pipe(gulp.dest('public/fonts'));
});

gulp.task('js', function() {
    gulp
        .src(['src/**/*.js', '!src/app/deps.js'])
        .pipe(order([
            'src/app/app.js',
            'src/**/*.js'
        ]))
        .pipe(concat('app.min.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(browserify())
        .pipe(uglify())
        .pipe(gulp.dest('public'));
});

gulp.task('sass', function() {
    return gulp.src('src/**/*.scss')
        .pipe(concat("app.min.css"))
        .pipe(sass({
            includePaths: ['node_modules']
        }).on('error', sass.logError))
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(gulp.dest('public'))
});

gulp.task('html', function() {
    gulp.src('src/app/app.html')
        .pipe(rename('index.html'))
        .pipe(gulp.dest('public'));
    gulp.src('src/**/template.html')
        .pipe(gulp.dest('public/views'));
});

gulp.task('img', function() {
    gulp.src('src/img/*.*')
        .pipe(gulp.dest('public/img'))
});

gulp.task('watch', function() {
    gulp.watch('src/app/deps.js', ['deps']);
    gulp.watch('src/**/*.js', ['js']);
    gulp.watch('src/**/*.scss', ['sass']);
    gulp.watch('src/**/*.html', ['html']);
    gulp.watch('src/img/', ['img'])
});

gulp.task('default', ['html', 'js', 'sass', 'img']);
